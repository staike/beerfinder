package es.bogdan.beerfinder.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import es.bogdan.beerfinder.R
import es.bogdan.beerfinder.data.model.BeerModel
import es.bogdan.beerfinder.databinding.ItemBeerBinding
import es.bogdan.beerfinder.util.doShortText

class BeerRecyclerViewAdapter( ): RecyclerView.Adapter<BeerRecyclerViewAdapter.BeerViewHolder>() {
    var listener: ((BeerModel)->Unit)? = null
    private var dataList = listOf<BeerModel>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BeerViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_beer, parent, false)
        return BeerViewHolder(view)
    }

    override fun onBindViewHolder(holder: BeerViewHolder, position: Int) {
        holder.bind(dataList[position])
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    fun updateData(newData: List<BeerModel>){
        dataList = newData
        notifyDataSetChanged()
    }


    inner class BeerViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val MAX_DESC_CHARS = 100
        val _viewBinding = ItemBeerBinding.bind(itemView)

        fun bind(item: BeerModel){
            _viewBinding.tvName.text = item.name
            _viewBinding.tvAlcohol.text = "Alc "+item.abv.toString()
            _viewBinding.tvDescription.text = doShortText(item.description,MAX_DESC_CHARS)
            _viewBinding.cv.setOnClickListener {
                listener?.invoke(item)
            }
        }
    }
}