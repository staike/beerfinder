package es.bogdan.beerfinder.view

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import es.bogdan.beerfinder.R
import es.bogdan.beerfinder.databinding.BeerFinderFragmentBinding
import es.bogdan.beerfinder.util.Status
import es.bogdan.beerfinder.view.adapter.BeerRecyclerViewAdapter
import es.bogdan.beerfinder.viewModel.BeerFinderViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import kotlin.concurrent.schedule

class BeerFinderFragment : Fragment() {

    private lateinit var _viewBinding: BeerFinderFragmentBinding
    private val _viewModel: BeerFinderViewModel by viewModel()
    private val _navigation: NavController by lazy { findNavController()}
    private var callTimer: Timer? = null
    private var adapter: BeerRecyclerViewAdapter? = null
    //oldText is variable used to prevent re-downloading data on fragment view recreation (navigation component) and
    //use the data stored in the ViewModel
    private var oldText = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _viewBinding = BeerFinderFragmentBinding.inflate(layoutInflater)
        return _viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setInfo()
        setObservers()
        setListeners()

    }

    private fun setInfo(){
        setAdapter()
    }
    private fun setListeners(){
        _viewBinding.etSearch.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                callTimer?.cancel()
            }

            override fun afterTextChanged(s: Editable?) {
                if (oldText != s.toString()){
                    //Using a timer to increase efficiency
                    callTimer = Timer()
                    callTimer?.schedule(300){
                        if (s.isNullOrBlank()){
                            _viewModel.getInitialBeers()
                        }else{
                            _viewModel.getBeerByName(s.toString())
                        }
                    }
                    oldText = s.toString()
                }
            }

        })

        //RecyclerView adapter listener
        adapter?.listener = {
            //Go to beer details fragment
            val direction = BeerFinderFragmentDirections.actionBeerFinderFragmentToBeerFinderDetailsFragment(it)
            _navigation.navigate(direction)
        }
    }

    private fun setObservers(){
        _viewModel.beerModelsLiveData.observe(viewLifecycleOwner){
            when(it.status){
                Status.LOADING -> isLoading(true)
                Status.SUCCESS -> {
                    isLoading(false)
                    it.data?.let {dat ->
                        if (dat.isNotEmpty()){
                            adapter?.updateData(dat)
                            _viewBinding.rv.scheduleLayoutAnimation()
                        }else{
                            _viewBinding.rv.scheduleLayoutAnimation()
                        }
                    }
                }
                Status.ERROR ->{
                    isLoading(false)
                    Snackbar.make(_viewBinding.root,getString(R.string.generic_error_network),Snackbar.LENGTH_LONG).show()
                    Log.e("BeerFinderFragment","Network Call: "+it.error)
                }
            }
        }
    }

    private fun isLoading(loading: Boolean){
        if (loading){
            _viewBinding.rv.visibility = View.GONE
            _viewBinding.progressBar.visibility = View.VISIBLE
        }else{
            _viewBinding.rv.visibility = View.VISIBLE
            _viewBinding.progressBar.visibility = View.GONE
        }
    }

    private fun setAdapter(){
        adapter = BeerRecyclerViewAdapter()
        _viewBinding.rv.layoutManager = LinearLayoutManager(requireContext())
        _viewBinding.rv.adapter = adapter
    }


}