package es.bogdan.beerfinder.view

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import es.bogdan.beerfinder.R
import es.bogdan.beerfinder.databinding.BeerFinderDetailsFragmentBinding
import es.bogdan.beerfinder.viewModel.BeerFinderDetailsViewModel


class BeerFinderDetailsFragment : Fragment() {

    private lateinit var _viewModel: BeerFinderDetailsViewModel
    private lateinit var _viewBinding: BeerFinderDetailsFragmentBinding
    private val itemArg: BeerFinderDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _viewBinding = BeerFinderDetailsFragmentBinding.inflate(layoutInflater)
        return _viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setInfo()
    }

    private fun setInfo(){
        with(itemArg.beerItem){
            Glide.with(requireContext()).load(image_url).placeholder(circularSpinnerDrawable()).into(
                _viewBinding.imageView
            )
            _viewBinding.tvName.text = name
            _viewBinding.tvAlcohol.text = "Alc $abv"
            _viewBinding.tvDescription.text = description
        }
    }


    private fun circularSpinnerDrawable(): Drawable{
        val drawable = CircularProgressDrawable(requireContext())
        drawable.setColorSchemeColors(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        drawable.centerRadius = 50f
        drawable.strokeWidth = 5f
        drawable.start()
        return drawable
    }
}