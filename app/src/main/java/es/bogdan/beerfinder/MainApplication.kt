package es.bogdan.beerfinder

import android.app.Application
import es.bogdan.beerfinder.di.appModule
import es.bogdan.beerfinder.di.repositoryModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MainApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        // Start Koin DI
        startKoin{
            androidLogger()
            androidContext(this@MainApplication)
            modules(appModule, repositoryModule)
        }
    }

}