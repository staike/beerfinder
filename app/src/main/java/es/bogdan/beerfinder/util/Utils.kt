package es.bogdan.beerfinder.util

/**
 * Method that replace white spaces with underscore (_)
 */
fun replaceBlankSpaces(source: String): String = source.replace(" ","_")

/**
 * Method that trim a text to make it shorter and add 3 dots to the end
 */
fun doShortText(source: String, size: Int): String {
    if (source.length > size){
        return source.substring(0,size)+"..."
    }else{
        return source
    }
}