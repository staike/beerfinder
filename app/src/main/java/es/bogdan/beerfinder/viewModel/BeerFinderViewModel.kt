package es.bogdan.beerfinder.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import es.bogdan.beerfinder.data.model.BeerModel
import es.bogdan.beerfinder.data.repository.IRepository
import es.bogdan.beerfinder.util.Resource
import es.bogdan.beerfinder.util.replaceBlankSpaces
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class BeerFinderViewModel(private val _repository: IRepository) : ViewModel() {
    private val _beerModelsMutLiveData = MutableLiveData<Resource<List<BeerModel>>>()
    val beerModelsLiveData: LiveData<Resource<List<BeerModel>>>
        get() = _beerModelsMutLiveData

    init {
        getInitialBeers()
    }

    fun getInitialBeers(){
        viewModelScope.launch(Dispatchers.IO) {
            //Loading state
            _beerModelsMutLiveData.postValue(Resource.loading(null))
            //Try catch to handle posible network errors
            try {
                val dataResult = _repository.getBeers()
                _beerModelsMutLiveData.postValue(Resource.success(dataResult))
            }catch (e: Exception){
                _beerModelsMutLiveData.postValue(Resource.error(e.toString(),null))
            }
        }
    }

    fun getBeerByName(beerName: String){
        viewModelScope.launch(Dispatchers.IO) {
            //Loading State
            _beerModelsMutLiveData.postValue(Resource.loading(null))
            //Try catch to handle posible network errors
            try {
                val dataResult = _repository.getBeerByName(replaceBlankSpaces(beerName))
                _beerModelsMutLiveData.postValue(Resource.success(dataResult))
            }catch (e: Exception){
                _beerModelsMutLiveData.postValue(Resource.error(e.toString(),null))
            }
        }
    }
    
}