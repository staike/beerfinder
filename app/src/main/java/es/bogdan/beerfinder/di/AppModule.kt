package es.bogdan.beerfinder.di

import es.bogdan.beerfinder.data.api.NetworkApi
import es.bogdan.beerfinder.data.repository.IRepository
import es.bogdan.beerfinder.data.repository.Repository
import es.bogdan.beerfinder.viewModel.BeerFinderDetailsViewModel
import es.bogdan.beerfinder.viewModel.BeerFinderViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val appModule = module {
    //Generic module
    viewModel { BeerFinderViewModel(get()) }
    viewModel { BeerFinderDetailsViewModel(get()) }

}

val repositoryModule = module {
    //Repository related module
    factory { provideRetrofitInstance() }
    factory { provideNetworkApi(get()) }
    single <IRepository> { Repository(get()) }

}

private fun provideRetrofitInstance(): Retrofit{
    val BASE_URL = "https://api.punkapi.com/v2/"
    return Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build()
}

private fun provideNetworkApi(retrofit: Retrofit): NetworkApi{
    return retrofit.create(NetworkApi::class.java)
}