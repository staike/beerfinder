package es.bogdan.beerfinder.data.repository

import es.bogdan.beerfinder.data.api.NetworkApi
import es.bogdan.beerfinder.data.model.BeerModel

class Repository(val networkApi: NetworkApi): IRepository {

    override suspend fun getBeers(): List<BeerModel> = networkApi.getBeers()
    override suspend fun getBeerByName(beerName: String): List<BeerModel> = networkApi.getBeerByName(beerName)
}