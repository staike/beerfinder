package es.bogdan.beerfinder.data.api

import es.bogdan.beerfinder.data.model.BeerModel
import retrofit2.http.GET
import retrofit2.http.Query

interface NetworkApi {

    @GET("beers")
    suspend fun getBeers(): List<BeerModel>

    @GET("beers")
    suspend fun getBeerByName(@Query("beer_name") beerName: String): List<BeerModel>
}