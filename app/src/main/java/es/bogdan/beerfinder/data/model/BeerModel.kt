package es.bogdan.beerfinder.data.model

import java.io.Serializable

data class BeerModel(
    val id: Int,
    val name: String,
    val description: String,
    val abv: Float,
    val image_url: String
    ): Serializable
