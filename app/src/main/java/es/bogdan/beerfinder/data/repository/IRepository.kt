package es.bogdan.beerfinder.data.repository

import es.bogdan.beerfinder.data.model.BeerModel

interface IRepository {
    suspend fun getBeers(): List<BeerModel>
    suspend fun getBeerByName(beerName: String): List<BeerModel>
}