package es.bogdan.beerfinder

import es.bogdan.beerfinder.util.doShortText
import es.bogdan.beerfinder.util.replaceBlankSpaces
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.*
import org.junit.Test

class UtilsTest {

    @Test
    fun utils_replaceBlankSpacesNoSpaces_returnOriginalString(){
        assertThat(replaceBlankSpaces("noBlankSpaces"), equalTo("noBlankSpaces"))
    }

    @Test
    fun utils_replaceBlankSpacesWithSpace_returnUnderscoreText(){
        assertThat(replaceBlankSpaces("with blank spaces"), equalTo("with_blank_spaces"))
    }

    @Test
    fun utils_replaceBlankSpacesTwoSpaces_returnDoubleUnderscoreText(){
        assertThat(replaceBlankSpaces("with  blank  spaces"), equalTo("with__blank__spaces"))
    }

    @Test
    fun utils_replaceBlankSpacesOnlySpaces_returnOnlyUnderscoreText(){
        assertThat(replaceBlankSpaces(" "), equalTo("_"))
    }

    @Test
    fun utils_doShortText_returnShortedTextWithDots(){
        assertThat(doShortText("Text to be shorted",10), equalTo("Text to be..."))
    }

    @Test
    fun utils_doShortTextTextNotEnougthLong_returnOriginalString(){
        assertThat(doShortText("Text to be shorted",30), equalTo("Text to be shorted"))
    }

    @Test
    fun utils_doShortTextBlankText_returnEmptyString(){
        assertThat(doShortText("",30), equalTo(""))
    }
}